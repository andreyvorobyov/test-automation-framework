package ta.trn.services.webui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import com.sigmaukraine.report.Log;

import ta.trn.services.webui.WebDriverManager;

public class MessagePage extends BasePage {

    private static final Logger LOGGER = Logger.getLogger(MessagePage.class);

    // message
    private static final By MESSAGE_PAGE_LINK = By.xpath("//*[@id='l_msg']//span[text()='My Messages']");
    private static final By COMPOSE_NEW_MESSAGE_BUTTON = By.xpath("//*[@id='im_write_btn']");
    private static final By RECEIVER_FIELD = By.xpath("//*[@id='imw_inp']");
    private static final By SEND_MESSAGE_BUTTON = By.xpath("//*[@id='imw_send']");
    private static final By TEXT_NEW_MESSAGE_FIELD = By.xpath("//*[@id='imw_editable']");
    private static final String TEXT_RECEIVED_MESSAGE_FIELD = "//*/div";
    private static final String SENDER_FIELD = "//*/table/tbody//a";

    // attach
    private static final By ADD_ATTACH_MENU = By.xpath("//*[@id='imw_attach']");
    private static final By ATTACH_DOCUMENT_BUTTON = By.xpath(".//*[@id='custom_menu_wrap']/div/div[3]//div[@class='add_media_items']/a[4]");
    private static final By ATTACH_FILE_FIELD = By.xpath("//*[@id='choose_photo_upload']");
    private static final By DOCUMENT_IS_ATTACHED_LABEL = By.xpath("//*/a[text()='Document']");

    public MessagePage(WebDriver driver) {
        super(driver);
    }

    public MessagePage sendMessage(String messageText, String receiverName) {
        Log.openSection("Send Message: text: "+messageText+", receiver: "+receiverName);
        WebDriverManager.fluentApi()
                .click(COMPOSE_NEW_MESSAGE_BUTTON)
                .sendKeys(RECEIVER_FIELD, receiverName)
                .sendKeys(RECEIVER_FIELD, Keys.ENTER)
                .sendKeys(TEXT_NEW_MESSAGE_FIELD, messageText)
                .click(SEND_MESSAGE_BUTTON);
        Log.closeSection();
        return this;
    }

    public MessagePage sendMessage(String messageText, String receiverName, String path) {
        Log.openSection("Send Message with picture: text: "+messageText+", receiver: "+receiverName);
        WebDriverManager.fluentApi()
                .click(COMPOSE_NEW_MESSAGE_BUTTON)
                .sendKeys(RECEIVER_FIELD, receiverName)
                .sendKeys(RECEIVER_FIELD, Keys.ENTER)
                .mouseOver(ADD_ATTACH_MENU)
                .waitElement(ATTACH_DOCUMENT_BUTTON)
                .click(ATTACH_DOCUMENT_BUTTON)
                .setVisible(ATTACH_FILE_FIELD)
                .sendKeys(ATTACH_FILE_FIELD, path)
                .waitElement(DOCUMENT_IS_ATTACHED_LABEL)
                .sendKeys(TEXT_NEW_MESSAGE_FIELD, messageText)
                .click(SEND_MESSAGE_BUTTON);
        Log.closeSection();
        return this;
    }

    public String messageDelivered(String messageText) {
        Log.openSection("Message delivered: "+messageText);
        String result = WebDriverManager.fluentApi()
                            .getInnerText(By.xpath(TEXT_RECEIVED_MESSAGE_FIELD + "[text() = '" + messageText + "']"));
        Log.closeSection();
        return result;
    }

    public String messageDeliveredWithAttach(String messageText) {
        Log.openSection("Message delivered with attach: "+messageText);
        String result = WebDriverManager.fluentApi()
                            .getInnerText(By.xpath(TEXT_RECEIVED_MESSAGE_FIELD + "[text() = '" + messageText + "']/following-sibling::div//a/span"));
        Log.closeSection();
        return result;
    }

    public MessagePage openMessageFrom(String sender) {
        Log.openSection("Open message from: "+sender);
        // optional
        try {
            WebDriverManager.fluentApi().click(By.xpath(SENDER_FIELD + "[text()='" + sender + "']/parent::div/parent::td/parent::tr/td[@class='dialogs_msg_contents']/div"));
        } catch (Exception exc) {
            // do nothing is optional step
            LOGGER.error(exc);
        }
        Log.closeSection();
        return this;
    }

    public MessagePage getBackToMessages() {
        Log.openSection("Get back to messages");
        WebDriverManager.fluentApi().click(MESSAGE_PAGE_LINK);
        Log.closeSection();
        return this;
    }
}
