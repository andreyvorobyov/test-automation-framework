package ta.trn.services.webui.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.sigmaukraine.report.Log;
import com.sigmaukraine.report.sourceProvider.TextData;

import ta.trn.services.error.FrameworkException;
import ta.trn.services.webui.WebDriverManager;

public class HomePage extends BasePage {

    // post with text
    private static final By TEXT_NEW_POST_FIELD = By.xpath("//*[@id='post_field']");
    private static final By SEND_POST_BUTTON = By.xpath("//*[@id='send_post']");
    private static final String TEXT_ADDED_POST_FIELD = "//*[@class='wall_post_text']";

    // for like
    private static final By ALL_POSTS_TEXT_COLLECTION = By.xpath("//*[@class='wall_post_text']");
    private static final By POST_DO_LIKE_BUTTON = By.xpath("//*[@class='post_full_like']/div[1]/span[1]");
    private static final By POST_IS_LIKED_LABEL = By.xpath("//*[@id='profile_wall']//div[@class='post_full_like']/div[1]/span[2]");

    // pages
    private static final By MESSAGE_PAGE_LINK = By.xpath("//*[@id='l_msg']//span[text()='My Messages']");
    private static final By POST_PAGE_LINK = By.xpath("//*[@id='page_wall_posts_count']");
    private static final By HOME_PAGE_LINK = By.xpath("//*[@class='top_home_link']");
    // post with attach
    private static final By ADD_ATTACH_MENU = By.xpath("//*[@id='custom_menu_cont']/div[2]");
    private static final By ATTACH_PHOTO_BUTTON = By.xpath("//*[@id='custom_menu_cont']/div[2]//div[@class='add_media_items']/a/nobr[text()='Photo']");
    private static final By ATTACH_FILE_FIELD = By.xpath("//*[@id='choose_photo_upload']");

    // other with use Robot class
    private static final By ADD_PICTURE_TO_POST_BUTTON = By.xpath("//*[@id='post_field_upload']");
 
    public HomePage(WebDriver driver) {
        super(driver);
    }

    public HomePage postOnWall(String text) {
        Log.openSection("Post on wall: " + text);
        WebDriverManager.fluentApi()
                .click(TEXT_NEW_POST_FIELD)
                .sendKeys(TEXT_NEW_POST_FIELD, text)
                .click(SEND_POST_BUTTON)
                .waitElement(By.xpath(TEXT_ADDED_POST_FIELD + "[text() = '" + text + "']"));
        Log.closeSection();
        return this;
    }

    public HomePage postOnWall(String text, String pathToPicture) {
        Log.openSection("Post on wall with picture");
        WebDriverManager.fluentApi()
                .click(POST_PAGE_LINK)
                .mouseOver(ADD_ATTACH_MENU)
                .waitElement(ATTACH_PHOTO_BUTTON)
                .click(ATTACH_PHOTO_BUTTON)
                .setVisible(ATTACH_FILE_FIELD)
                .sendKeys(ATTACH_FILE_FIELD, pathToPicture)
                .click(TEXT_NEW_POST_FIELD)
                .sendKeys(TEXT_NEW_POST_FIELD, text)
                .click(SEND_POST_BUTTON)
                .waitElement(By.xpath(TEXT_ADDED_POST_FIELD + "[text() = '" + text + "']"));
        Log.closeSection();
        return this;
    }

    public HomePage postOnWallUseRobot(String text, String pathToPicture) {
        Log.info("Post on wall use Robot class", text, new TextData(pathToPicture));
        WebDriverManager.fluentApi()
                .click(TEXT_NEW_POST_FIELD)
                .click(ADD_PICTURE_TO_POST_BUTTON);

        Robot robot = null;
        try {
            robot = new Robot();
        } catch (AWTException exc) {
            throw new FrameworkException("Could not create awtRobot", exc);
        }
        robot.delay(500);

        Toolkit.getDefaultToolkit()
                .getSystemClipboard()
                .setContents(new StringSelection(pathToPicture), null);

        robot.keyPress(KeyEvent.VK_ENTER);
        robot.delay(500);
        robot.keyRelease(KeyEvent.VK_ENTER);
        robot.delay(500);
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.delay(500);
        robot.keyPress(KeyEvent.VK_V);
        robot.delay(500);
        robot.keyRelease(KeyEvent.VK_V);
        robot.delay(500);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.delay(500);
        robot.keyPress(KeyEvent.VK_TAB);
        robot.delay(500);
        robot.keyRelease(KeyEvent.VK_TAB);
        robot.delay(500);
        robot.keyPress(KeyEvent.VK_TAB);
        robot.delay(500);
        robot.keyRelease(KeyEvent.VK_TAB);
        robot.delay(500);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.delay(500);
        robot.keyRelease(KeyEvent.VK_ENTER);
        robot.delay(3000);
        return postOnWall(text);
    }

    public HomePage likeWallPost(int number) {
        Log.openSection("Like Post by number: " + number);
        driver.findElements(POST_DO_LIKE_BUTTON)
                .get(number)
                .click();
        Log.closeSection();
        return this;
    }

    public int getCountPostsOnWall() {
        Log.openSection("Get count post on wall");
        int result = driver.findElements(ALL_POSTS_TEXT_COLLECTION).size();
        Log.info("count Posts: "+result);
        Log.closeSection();
        return result;
    }

    public String getTextFromPostByNumber(int number) {
        Log.openSection("Get text from Post by number: " + number);
        String result = driver.findElements(ALL_POSTS_TEXT_COLLECTION)
                            .get(getCountPostsOnWall() - number)
                            .getText();
        Log.info("text", null, new TextData(result));
        Log.closeSection();
        return result;
    }

    public boolean isNotLikedPostByNumber(int number) {
        Log.openSection("Is not liked Post by number: "+number);
        boolean result = driver.findElements(POST_IS_LIKED_LABEL)
                            .get(number)
                            .getText()
                            .isEmpty();
        Log.info("Is not Liked: "+result);
        Log.closeSection();
        return result;
    }

    public MessagePage openMessages() {
        Log.info("Open Messages");
        WebDriverManager.fluentApi().click(MESSAGE_PAGE_LINK);
        return new MessagePage(driver);
    }
    
    public HomePage openHomePage() {
        Log.openSection("Open Home page");
        WebDriverManager.fluentApi().click(HOME_PAGE_LINK);
        Log.closeSection();
        return this;
    }
}
