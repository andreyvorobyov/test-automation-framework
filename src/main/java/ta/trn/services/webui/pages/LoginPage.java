package ta.trn.services.webui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.sigmaukraine.report.Log;

import ta.trn.TestConfig;
import ta.trn.services.webui.WebDriverManager;

public class LoginPage extends BasePage {

    private static final Logger LOGGER = Logger.getLogger(LoginPage.class);

    private static final By USER_NAME_FIELD = By.xpath("//*[@id='quick_email']");
    private static final By PASSWORD_FIELD = By.xpath("//*[@id='quick_pass']");
    private static final By LOGIN_BUTTON = By.xpath("//*[@id='quick_login_button']");
    private static final By IS_LOGIN_LABEL = By.xpath("//*[@id='profile_online_lv']");

    private static final By OPEN_SELECT_LOCALE_BUTTON = By.xpath("//*[@id='footer_wrap']//div/a[5][@class='bnav_lang']");
    private static final By CLOSE_SELECT_LOCALE_BUTTON = By.xpath("//*[@id='box_layer']//button");
    private static final By ENGLISH_LOCALE_BUTTON = By.xpath("//*[@id='box_layer']//div/a[text()='English']");
        
    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage open() {
        Log.info("Open Login page");
        driver.get(TestConfig.WebUI.BASE_URL);
        Log.closeSection();
        return this;
    }

    public LoginPage changeLocaleToEnglish(){
       Log.openSection("Change locale to English"); 
       if (WebDriverManager.fluentApi().getTitle().contains("Welcome")){
           Log.closeSection();
           return this;
       }
       
       WebDriverManager.fluentApi().click(OPEN_SELECT_LOCALE_BUTTON);
       try{
           WebDriverManager.fluentApi()
                   .waitElement(ENGLISH_LOCALE_BUTTON)
                   .click(ENGLISH_LOCALE_BUTTON);
       }catch(Exception exc){
           LOGGER.error(exc);
           WebDriverManager.fluentApi().click(CLOSE_SELECT_LOCALE_BUTTON);
       }               
       WebDriverManager.fluentApi().waitElement(USER_NAME_FIELD);
       Log.closeSection();
       return this;
    }
    
    public LoginPage enterCredentials(String login, String password) {
        Log.openSection("Enter Credentials");
        WebDriverManager.fluentApi()
                .sendKeys(USER_NAME_FIELD, login)
                .sendKeys(PASSWORD_FIELD, password);
        Log.closeSection();
        return this;
    }

    public LoginPage pressLogin() {
        Log.openSection("Press Login");
        WebDriverManager.fluentApi().click(LOGIN_BUTTON);
        Log.closeSection();
        return this;
    }

    public boolean isLoginSuccesfull() {
        Log.openSection("Is Login Succesfull");
        try {
            return WebDriverManager.fluentApi()
                    .waitElement(IS_LOGIN_LABEL)
                    .getText(IS_LOGIN_LABEL)
                    .contains("Online");
        } catch (Exception exc) {
            LOGGER.error(exc);
        }
        Log.closeSection();
        return false;
    }
}
