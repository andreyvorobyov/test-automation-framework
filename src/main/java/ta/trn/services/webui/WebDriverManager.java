package ta.trn.services.webui;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.sigmaukraine.report.Log;

import ta.trn.TestConfig;
import ta.trn.services.error.FrameworkException;

public class WebDriverManager {

    private static WebDriver webDriver = null;

    private WebDriverManager() {
    }

    private static WebDriver setBrowser() {
        Log.info("Set Browser: "+TestConfig.WebUI.BROWSER);
        WebDriver driver = null;
        switch (TestConfig.WebUI.BROWSER) {
        case "firefox":
            driver = new FirefoxDriver();
            break;
        case "chrome":
            driver = new ChromeDriver();
            break;
        case "opera":
            driver = new OperaDriver();
            break;
        case "remote.chrome":
            driver = createRemoteDriverChrome();
            break;
        case "remote.ie":
            driver = createRemoteDriverIE();
            break;
        default:
            throw new FrameworkException("Could not set browser: " + TestConfig.WebUI.BROWSER, null);
        }
        driver.manage().timeouts().implicitlyWait(TestConfig.WebUI.IMPLICIT_WAIT_TIMEOUT, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(TestConfig.WebUI.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
        return driver;
    }

    private static URL createBrowserExecutor(){
        URL executorURL = null;
        try {
            executorURL = new URL(TestConfig.WebUI.BROWSER_EXECUTOR);
        } catch (MalformedURLException exc) {
            throw new FrameworkException("Could not create browser executor URL", exc);
        }
        return executorURL;
    }
    
    private static WebDriver createRemoteDriverChrome(){
        ChromeOptions options = new ChromeOptions();
        DesiredCapabilities dc = DesiredCapabilities.chrome();
        dc.setCapability(ChromeOptions.CAPABILITY, options);      
        return new RemoteWebDriver(createBrowserExecutor(), dc);
    }
    
    private static WebDriver createRemoteDriverIE(){
        DesiredCapabilities dc = DesiredCapabilities.internetExplorer();
        dc.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
        return new RemoteWebDriver(createBrowserExecutor(),dc);        
    }
    
    public static synchronized WebDriver initDriver() {
        if (webDriver == null) {
            webDriver = setBrowser();
        }
        return webDriver;
    }

    public static synchronized void closeDriver() {
        webDriver.quit();
        webDriver = null;
        Log.info("Close Browser");
    }

    public static WebDriverManager fluentApi() {
        return new WebDriverManager();
    }

    public WebDriverManager click(By locator) {
        Log.info("Click by", locator.toString());
        waitElementToBeClickable(locator).click();
        return this;
    }

    public WebDriverManager sendKeys(By locator, String text) {
        Log.info("Send keys: " + text, "locator: " + locator.toString());
        waitElementToBeClickable(locator).sendKeys(text);
        return this;
    }

    public WebDriverManager sendKeys(By locator, Keys keys) {
        Log.info("Send keys: "+keys.toString(), "locator: " + locator.toString());
        waitElementToBeClickable(locator).sendKeys(keys);
        return this;
    }

    public WebDriverManager waitElement(By locator) {
        Log.info("Wait element", locator.toString());
        waitElementLocated(locator);
        return this;
    }

    public WebDriverManager mouseOver(By locator) {
        Log.info("Mouse over", locator.toString());
        String code = "var fireOnThis = arguments[0];" + "var evObj = document.createEvent('MouseEvents');"
                + "evObj.initEvent( 'mouseover', true, true );" + "fireOnThis.dispatchEvent(evObj);";
        ((JavascriptExecutor) webDriver).executeScript(code, waitElementLocated(locator));
        return this;
    }

    public WebDriverManager setVisible(By locator) {
        Log.info("Set visible", locator.toString());
        String code = "arguments[0].setAttribute(\"style\",\"visibility: visible; display: block;\");";
        ((JavascriptExecutor) webDriver).executeScript(code, waitElementLocated(locator));
        return this;
    }

    public String getText(By locator) {
        String result = waitElementLocated(locator).getText();
        Log.info("Get text", result);
        return result;
    }

    public String getInnerText(By locator) {
        String result = waitElementLocated(locator).getAttribute("innerText");
        Log.info("Get inner text", result);
        return result;
    }

    public String getTitle(){
        String result = webDriver.getTitle();
        Log.info("Get title", result);
        return result;
    }
    
    private static void waitElement(ExpectedCondition<WebElement> condition) {
        webDriver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        try {
            WebDriverWait wait = new WebDriverWait(webDriver, TestConfig.WebUI.ELEMENT_WAIT_TIMEOUT);
            wait.until(condition);
        } catch (TimeoutException exc) {
            throw exc;
        } finally {
            webDriver.manage().timeouts().implicitlyWait(TestConfig.WebUI.IMPLICIT_WAIT_TIMEOUT, TimeUnit.SECONDS);
        }
    }

    private static synchronized WebElement waitElementToBeClickable(By locator) {
        waitElement(ExpectedConditions.elementToBeClickable(locator));
        return webDriver.findElement(locator);
    }

    private static synchronized WebElement waitElementLocated(By locator) {
        waitElement(ExpectedConditions.presenceOfElementLocated(locator));
        return webDriver.findElement(locator);
    }
}
