package ta.trn.services;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import ta.trn.services.error.FrameworkException;

public class PropertyManager {

    private final Properties properties = loadProperties("properties/default.properties");

    public  Properties getCurrentProperties(){
        return properties;
    }
    
    protected static synchronized Properties loadProperties(String path) throws FrameworkException {
        try {
            Properties prop = new Properties();
            prop.load(FileSystemManager.retrieveResource(path));
            return prop;
        } catch (Exception exc) {
            throw new FrameworkException("Could not read proterty file: "+path, exc);
        }
    }

    protected String getProperty(String key, String defaultValue){ 
        //get property from JVM
        String value = System.getProperty(key, defaultValue);
        if(value.isEmpty() || value.equals(defaultValue)){
            return getCurrentProperties().getProperty(key, defaultValue);
        }
        else{
            return value;
        }
    }
    
    protected  String getProperty(String key) {
        return getProperty(key, "");
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    protected  Map<String, String> getProperties() {
        return new HashMap(getCurrentProperties());
    }
}
