package ta.trn.services.error;

public class FrameworkException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public FrameworkException(String message, Throwable cause) {
        super(message, cause);
    }
}
