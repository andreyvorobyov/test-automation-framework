package ta.trn.services.webapi;

import java.net.URI;
import java.net.URISyntaxException;

import ta.trn.TestConfig;
import ta.trn.services.error.FrameworkException;

public class UriConstants {

    public static final URI POSTS = createURI(TestConfig.WebApi.HOST + "/posts");

    private UriConstants() {
    }

    private static URI createURI(String url) {
        try {
            return new URI(url);
        } catch (URISyntaxException exc) {
            throw new FrameworkException("Coul not create uri from string", exc);
        }
    }
}
