package ta.trn.services.webapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public class Post {

    @JsonProperty("userId")
    private Integer userId;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("body")
    private String body;

    public static Post createPost(Integer userId, String title, String body) {
        Post post = new Post();
        post.setUserId(userId);
        post.setTitle(title);
        post.setBody(body);
        return post;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getTitle(), getBody());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Post other = (Post) obj;
        return Objects.equal(this.getTitle(), other.getTitle()) && Objects.equal(this.getBody(), other.getBody());
    }

    @Override
    public String toString() {

        return MoreObjects.toStringHelper(this)
                .add("UserId", getUserId())
                .add("Id", getId())
                .add("Title", getTitle())
                .add("Body", getBody())
                .toString();
    }
}
