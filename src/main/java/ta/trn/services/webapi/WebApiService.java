package ta.trn.services.webapi;

import java.net.URI;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.sigmaukraine.report.Log;
import com.sigmaukraine.report.sourceProvider.TextData;

import ta.trn.services.error.FrameworkException;

public class WebApiService {

    public Response sendGetRequestWithQueryParam(URI uri, Map<String, String> parameters){
        Log.info("GET Request (query param): "+ uri.toString(), "Parameters: " + parameters.toString());
        Response response = RestAssured.given()
                                .parameters(parameters)
                                .get(uri);
        Log.info("Response status code: " + response.getStatusCode(), null, new TextData(response.asString()));
        return response;
    }

    public Response sendGetRequestWithPathParam(URI uri, List<String> parameters){
        URI uriWithParams = createUriWithPathParam(uri, parameters);
        Log.info("GET Request (path param) :" + uriWithParams.toString());
        Response response = RestAssured.get(uriWithParams);
        Log.info("GET Response status code: " + response.getStatusCode(), null, new TextData(response.asString()));
        return response;
    }

    public Response sendPostRequest(URI uri, String jsonText){
        Log.info("POST Request: " + uri.toString(), null, new TextData(jsonText));
        Response response = RestAssured.given()
                                 .contentType(ContentType.JSON)
                                 .body(jsonText)
                                 .when()
                                 .post(uri);
        Log.info("POST Response status code: " + response.getStatusCode(), null, new TextData(response.asString()));
        return response;
    }

    public Response sendDeleteRequest(URI uri, List<String> parameters){
        URI uriWithParams = createUriWithPathParam(uri, parameters);
        Log.info("DELETE Request: " + uriWithParams.toString());
        Response response = RestAssured.given().delete(uriWithParams);
        Log.info("DELETE Response status code: " + response.getStatusCode(), null, new TextData(response.asString()));
        return response;
    }

    public <T> String serealizeToJson(T pojo) {        
        try {
            return new ObjectMapper().writeValueAsString(pojo);            
        } catch (JsonProcessingException exc) {
            throw new FrameworkException("Could not write to JSON", exc);
        }
    }

    @SuppressWarnings("unchecked")
    public <T> T deserealizeFromJson(TypeReference<T> type, String json) {
        try {
            return (T) new ObjectMapper().readValue(json, type);
        } catch (Exception exc) {
            Log.error("Could not parse from JSON", exc);
            throw new FrameworkException("Could not parse from JSON", exc);
        }
    }
        
    private static URI createUriWithPathParam(URI uri, List<String> parameters) {        
        String uriString = uri.toString();
        for (String param : parameters) {
            uriString += "/" + param;
        }
        return URI.create(uriString);
    }
}
