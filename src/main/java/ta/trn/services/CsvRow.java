package ta.trn.services;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

public class CsvRow implements Iterator<CsvRow> {

    private int index;
    private List<Map<String, Object>> data;

    public CsvRow(List<Map<String, Object>> data) {
        index = 0;
        this.data = data;
    }

    public CsvRow(Integer index, List<Map<String, Object>> data) {
        this.data = data;
        this.index = index;
    }

    @Override
    public boolean hasNext() {
        return index < data.size()-1;
    }

    @Override
    public CsvRow next () {
        index++;
        if(index >= data.size()){
            throw new NoSuchElementException();
        }
        return this;
    }

    @Override
    public void remove(){
    	throw new UnsupportedOperationException();
    }
    
    public Object get(String columnName) {
        return data.get(index).get(columnName);
    }

}
