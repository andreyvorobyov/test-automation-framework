package ta.trn.services.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.sigmaukraine.report.Log;
import com.sigmaukraine.report.sourceProvider.TextData;

import ta.trn.TestConfig;
import ta.trn.services.CsvData;
import ta.trn.services.error.FrameworkException;

public class DataBaseManager {

    private Connection connection;

    private final String CONNECTION_URL = "jdbc:mysql://" + TestConfig.DataBase.HOST + ":" 
                                          + TestConfig.DataBase.PORT + "/" + TestConfig.DataBase.DB_NAME;

    public void connect() {
        Log.info("Connecting: "+CONNECTION_URL);
        try {
            this.connection = DriverManager.getConnection(CONNECTION_URL, TestConfig.DataBase.USER_NAME,
                    TestConfig.DataBase.PASS);

        } catch (SQLException exc) {
            throw new FrameworkException("Connection failed", exc);
        }
    }

    public void disconnect() {
        Log.info("Disconnecting");
        try {
            connection.close();
        } catch (SQLException exc) {
            throw new FrameworkException("Connection failed", exc);
        }
    }

    public ResultSet query(String query) {
        Log.info("Query", null, new TextData(query));
        try {
            Statement statement = connection.createStatement();
            return statement.executeQuery(query);
        } catch (SQLException exc) {
            throw new FrameworkException("Could not execute query", exc);
        }
    }

    public void closeResultSet(ResultSet resultSet){
        try {
            resultSet.close();
        } catch (SQLException exc) {
            throw new FrameworkException("Could not close result set", exc);
        }
    }
    
    public void command(String query) {
        Log.info("Command", null, new TextData(query));
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(query);
        } catch (SQLException exc) {
            throw new FrameworkException("Could not execute command", exc);
        }
    }

    public List<Map<String, Object>> convertResultSetToList(ResultSet resultSet, String... fields) {
        List<Map<String, Object>> collectedResult = new ArrayList<>();
        try {
            while (resultSet.next()) {
                Map<String, Object> resultRow = new TreeMap<>();
                for (int i = 0; i < fields.length; i++) {
                    resultRow.put(fields[i], resultSet.getObject(fields[i]));
                }
                collectedResult.add(resultRow);
            }
        } catch (SQLException exc) {
            throw new FrameworkException("Could not convert ResultSet", exc);
        }
        CsvData csvData = new CsvData(collectedResult);
        csvData.setPrintHeaders(false);
        Log.info("Result", null, new TextData(csvData.toString()));
        return collectedResult;
    }
}
