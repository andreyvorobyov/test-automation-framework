package ta.trn.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class CsvData implements Iterable<CsvRow> {

    private List<Map<String, Object>> data;

    private boolean printHeaders = true;
    
    public CsvData() {
        data = new ArrayList<Map<String, Object>>();
    }

    public CsvData(List<Map<String, Object>> data) {        
        this.data = data;
    }

    @Override
    public CsvRow iterator() {
        return new CsvRow(data);
    }

    public CsvRow getRow(Integer index) {
        return new CsvRow(index, data);
    }

    public List<String> getColumnNames() {
        List<String> colunmNames = new ArrayList<>();
        Map<String, Object> row = data.get(0);
        for (Entry<String, Object> entry : row.entrySet()) {
            colunmNames.add(entry.getKey());
        }
        return colunmNames;
    }

    public void setPrintHeaders(boolean value){
        this.printHeaders = value;
    }
    
    public boolean getPrintHeaders(){
        return this.printHeaders;
    }
    
    @Override
    public String toString() {
        String result = "";
        Map<String, Integer> columnWidth = calculateMaxColumnWidth(this.data);
        boolean isFirstLine = true;
        for (Map<String, Object> row : data) {
            String line = "";
            for (Entry<String, Object> entry : row.entrySet()) {
                String format = "%-" + (columnWidth.get(entry.getKey()) + 1) + "s";
                line += String.format(format, entry.getValue().toString().trim());
                line += "|";
            }
            if (isFirstLine && printHeaders) {
                isFirstLine = false;
                String delimiterLine = createDelimiterLine(line);
                result += delimiterLine + "\n";
                result += line + "\n";
                result += delimiterLine + "\n";
            } else {
                result += line + "\n";
            }
        }
        return result;
    }

    private Map<String, Integer> calculateMaxColumnWidth(List<Map<String, Object>> data) {
        Map<String, Integer> result = new HashMap<>();
        for (Map<String, Object> row : data) {
            for (Entry<String, Object> entry : row.entrySet()) {
                String key = entry.getKey();
                result.put(key, Math.max(entry.getValue().toString().length(), result.get(key) == null ? 0 : result.get(key)));
            }
        }
        return result;
    }

    private static String createDelimiterLine(String line) {
        String delimiterLine = "";
        for (int i = 0; i < line.length(); i++) {
            delimiterLine += "-";
        }
        return delimiterLine;
    }
}
