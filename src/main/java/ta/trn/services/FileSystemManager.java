package ta.trn.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.IOUtils;

import ta.trn.services.error.FrameworkException;

public class FileSystemManager {

    private FileSystemManager() {
    }

    public static List<List<String>> readCSVFile(InputStream inStream, String delimiter) {
        try (BufferedReader bufReader = new BufferedReader(new InputStreamReader(inStream))) {
            List<List<String>> result = new ArrayList<>();
            String line;
            while ((line = bufReader.readLine()) != null) {
                result.add(Arrays.asList(line.split(delimiter)));
            }
            return result;
        } catch (Exception exc) {
            throw new FrameworkException("Could not read csv file", exc);
        }
    }
    
    public static List<Map<String, Object>> readCSVFileAsMap(InputStream inStream, String delimiter) {
        try (BufferedReader bufReader = new BufferedReader(new InputStreamReader(inStream))) {
            List<Map<String, Object>> result = new ArrayList<>();
            String line;
            List<String> keys = new ArrayList<>();
            // first line is keys
            boolean isFirstLine = true;
            while ((line = bufReader.readLine()) != null) {
                // get keys from first line
                if (isFirstLine) {
                    isFirstLine = false;
                    keys = Arrays.asList(line.split(delimiter));
                    //first row contain key names
                    result.add(createCsvRow(keys, keys));
                    continue;
                }
                result.add(createCsvRow(keys, Arrays.asList(line.split(delimiter))));
            }
            return result;
        } catch (Exception exc) {
            throw new FrameworkException("Could not read csv file", exc);
        }
    }

    public static CsvData readCSVFileAsCsvData(String path, String delimiter) {
        return new CsvData(readCSVFileAsMap(retrieveResource(path), delimiter));
    }
    
    public static List<List<String>> readCSVFile(String path, String delimiter) {
        return readCSVFile(retrieveResource(path), delimiter);
    }

    public static List<Map<String, Object>> readCSVFileAsMap(String path, String delimiter) {
        return readCSVFileAsMap(retrieveResource(path), delimiter);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static Map<String, String> readPropertyFile(String path) {
        try {
            Properties properties = new Properties();
            properties.load(retrieveResource(path));
            return new HashMap(properties);
        } catch (Exception exc) {
            throw new FrameworkException("Could not read property file: " + path, exc);
        }
    }
    
    public static String readTextFile(String path) {
        try (StringWriter writer = new StringWriter()) {
            IOUtils.copy(retrieveResource(path), writer, StandardCharsets.UTF_8.name());
            return writer.toString();
        } catch (IOException exc) {
            throw new FrameworkException("Could not read text file", exc);
        }
    }
    
    public static InputStream retrieveResource(String path) {
        InputStream stream = FileSystemManager.class.getClassLoader().getResourceAsStream(path);
        if (stream == null) {
            throw new FrameworkException("Resource not found: " + path, null);
        }
        return stream;
    }
    
    public static String getAbsolutePathToResource() {
        return removeLastChar(new File(".").getAbsolutePath().replace("\\", "/")) + "src/main/resources/";
    }
    
    private static String removeLastChar(String oldString) {
        return oldString.substring(0, oldString.length() - 1);
    }
    
    private static Map<String, Object> createCsvRow(List<String> keys, List<String> values){
        Map<String, Object> row = new HashMap<>();
        int keyIndex = 0;
        for (String value : values) {
            row.put(keys.get(keyIndex), value);
            keyIndex++;
        }
        return row;
    }    
}
