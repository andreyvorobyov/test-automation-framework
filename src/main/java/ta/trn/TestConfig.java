package ta.trn;

import java.util.Properties;

import ta.trn.services.PropertyManager;

public class TestConfig extends PropertyManager {

    private Properties properties = null;

    public TestConfig(String path) {
        this.properties = loadProperties(path);
    }

    public TestConfig() {
        properties = super.getCurrentProperties();
    }

    @Override
    public Properties getCurrentProperties() {
        return properties;
    }

    public static final class WebUI {

        private static final TestConfig TEST_CONFIG = new TestConfig("properties/webdriver.properties");

        public static final String BROWSER = TEST_CONFIG.getProperty("browser", "");
        public static final String BASE_URL = TEST_CONFIG.getProperty("base_url", "");
        public static final Integer ELEMENT_WAIT_TIMEOUT = Integer.parseInt(TEST_CONFIG.getProperty("element_wait_timeout_sec", "0"));
        public static final Integer PAGE_LOAD_TIMEOUT = Integer.parseInt(TEST_CONFIG.getProperty("page_load_timeout_sec", "0"));
        public static final Integer IMPLICIT_WAIT_TIMEOUT = Integer.parseInt(TEST_CONFIG.getProperty("implicit_wait_timeout_sec", "0"));
        public static final String USER_NAME = TEST_CONFIG.getProperty("user_name", "");
        public static final String PASS = TEST_CONFIG.getProperty("pass", "");
        public static final String BROWSER_EXECUTOR = TEST_CONFIG.getProperty("browser_executor", "");

        private WebUI() {
        }
    }
    
    public static final class WebApi {

        private static final TestConfig TEST_CONFIG = new TestConfig("properties/webapi.properties");

        public static final String HOST = TEST_CONFIG.getProperty("host", "");

        private WebApi() {
        }
    }
    
    public static final class DataBase {

        private static final TestConfig TEST_CONFIG = new TestConfig("properties/database.properties");

        public static final String HOST = TEST_CONFIG.getProperty("host", "");
        public static final String PORT = TEST_CONFIG.getProperty("port", "");
        public static final String DB_NAME = TEST_CONFIG.getProperty("db_name", "");
        public static final String USER_NAME = TEST_CONFIG.getProperty("username", "");
        public static final String PASS = TEST_CONFIG.getProperty("pass", "");

        private DataBase() {
        }
    }
}
