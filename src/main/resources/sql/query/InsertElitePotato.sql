#UPDATE vorobyov.products 
#   SET products.product_name = CONCAT('Elite ',product_name),
#       products.price = price * 20
# WHERE product_name = 'Potato';

INSERT INTO vorobyov.products(product_name, price, description) 
     SELECT CONCAT('ELITE_',origin.product_name) , origin.price * 20 , origin.description
       FROM vorobyov.products as origin
      WHERE origin.product_name = 'Potato'