SELECT CONCAT (customers.first_name,' ',customers.last_name) AS customer_name, 
       ordered_products.amount,
       DATE_FORMAT(TIMESTAMP(purchase_orders.order_date, purchase_orders.order_time),'%d/%m/%y  %h:%m') AS order_date 
  FROM vorobyov.ordered_products
      LEFT JOIN vorobyov.purchase_orders
      ON ordered_products.order_id = purchase_orders.order_id
      LEFT JOIN vorobyov.customers
      ON purchase_orders.customer_id = customers.customer_id
 WHERE ordered_products.product_id IN
     (SELECT products.product_id
        FROM vorobyov.products
       WHERE product_name = 'Rice')  
  ORDER BY TIMESTAMP(purchase_orders.order_date, purchase_orders.order_time)