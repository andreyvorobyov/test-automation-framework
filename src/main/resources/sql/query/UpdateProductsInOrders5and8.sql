UPDATE vorobyov.products SET product_name = CONCAT(product_name, '_updated')
WHERE  product_id IN
  (SELECT ordered_products.product_id
     FROM vorobyov.ordered_products
    WHERE order_id = 5 
       OR order_id = 8);