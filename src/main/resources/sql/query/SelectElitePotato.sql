#SELECT product_name, price
#  FROM vorobyov.products
# WHERE products.product_name = 'ELITE_Potato'

SELECT origin.product_name   AS origin_name,
        origin.price         AS origin_price,
        updated.product_name AS product_name, 
        updated.price        AS price
   FROM vorobyov.products as origin
  INNER JOIN vorobyov.products as updated
     ON origin.product_name = 'Potato'
    AND updated.product_name = 'ELITE_Potato'