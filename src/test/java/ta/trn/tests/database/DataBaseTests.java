package ta.trn.tests.database;

import static org.hamcrest.MatcherAssert.assertThat;

import java.io.File;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hamcrest.Matchers;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import com.sigmaukraine.report.Log;
import com.sigmaukraine.report.sourceProvider.CSVData;

import ta.trn.services.CsvData;
import ta.trn.services.CsvRow;
import ta.trn.services.FileSystemManager;
import ta.trn.services.database.DataBaseManager;
import ta.trn.tests.Watcher;

public class DataBaseTests {

    private static DataBaseManager dbManager;

    private static final String CUSTOMER_NAME = "customer_name";
    private static final String CUSTOMER_ID = "customer_id";
    private static final String CUSTOMER_FIRST_NAME = "first_name";
    private static final String CUSTOMER_LAST_NAME = "last_name";
    
    private static final String PRODUCT_NAME =  "product_name";
    
    @Rule 
    public TestName testName = new TestName();

    @Rule
    public Watcher watcher = new Watcher();
    
    @BeforeClass
    public static void connect() {
        Log.openTestSuite(DataBaseTests.class.getName());

        Log.openSection("Preconditions");        
        dbManager = new DataBaseManager();
        dbManager.connect();
        Log.closeSection();        
    }

    @AfterClass
    public static void disconnect() {
        dbManager.disconnect();
        Log.closeTestSuite();
    }

    @Before
    public void restore() {
        Log.openTestCase(testName.getMethodName());
        Log.openSection("Delete Data");
        // delete data
        dbManager.command(FileSystemManager.readTextFile("sql/query/restore/delete-ordered_product.sql"));
        dbManager.command(FileSystemManager.readTextFile("sql/query/restore/delete-purchase_orders.sql"));
        dbManager.command(FileSystemManager.readTextFile("sql/query/restore/delete-products.sql"));
        dbManager.command(FileSystemManager.readTextFile("sql/query/restore/delete-customers.sql"));
        Log.closeSection();
        Log.openSection("Load Data");
        String pathToDataFiles = FileSystemManager.getAbsolutePathToResource() + "sql/query/restore/data/";
        // load data
        
        String pathParam = "{path}";
        dbManager.command(FileSystemManager.readTextFile("sql/query/restore/load-customers.sql").replace(pathParam,pathToDataFiles));
        dbManager.command(FileSystemManager.readTextFile("sql/query/restore/load-products.sql").replace(pathParam,pathToDataFiles));
        dbManager.command(FileSystemManager.readTextFile("sql/query/restore/load-purchase_orders.sql").replace(pathParam,pathToDataFiles));
        dbManager.command(FileSystemManager.readTextFile("sql/query/restore/load-ordered_product.sql").replace(pathParam,pathToDataFiles));
        Log.closeSection();
        
    }
    
    @Test
    public void customersHaveOrderedRiceIsNotEmpty() {
        //get customers have ordered rice
        ResultSet resultSet = dbManager.query(FileSystemManager.readTextFile("sql/query/SelectCustomersHaveOrderedRice.sql"));
        List<Map<String, Object>> result = dbManager.convertResultSetToList(resultSet, CUSTOMER_NAME);
        
        //extract customers name
        List<String> customerNames = new ArrayList<>();
        for (int i = 0; i < result.size(); i++) {
            customerNames.add((String) result.get(i).get(CUSTOMER_NAME));
        }
        dbManager.closeResultSet(resultSet);
        
        //has some customers ordered rice
        Assert.assertTrue(!customerNames.isEmpty());
    }

    @Test
    public void updateProductsInOrders5and8AndAssertWhatUpdated() {
        //update product
        dbManager.command(FileSystemManager.readTextFile("sql/query/UpdateProductsInOrders5and8.sql"));
        
        //select updated product
        ResultSet resultSet = dbManager.query(FileSystemManager.readTextFile("sql/query/SelectProductsInOrders5and8.sql"));
        List<Map<String, Object>> result = dbManager.convertResultSetToList(resultSet, PRODUCT_NAME);
       
        //convert selected result to array with product names
        List<String> actualProducts = new ArrayList<>();
        for (int i = 0; i < result.size(); i++) {
            actualProducts.add((String) result.get(i).get(PRODUCT_NAME));
        }
        
        //expected array and actual array is equal
        assertThat(actualProducts, Matchers.containsInAnyOrder("Rice_updated", "Buckwheat_updated", "Wheat_updated", "Oats_updated", "Potato_updated"));                
        
        dbManager.closeResultSet(resultSet);
    }

    @Test
    public void insertElitePotatoAndAssertWhatUpdated() {
      //update potato to elite potato
        dbManager.command(FileSystemManager.readTextFile("sql/query/InsertElitePotato.sql"));
        //select inserted potato
        ResultSet resultSet = dbManager.query(FileSystemManager.readTextFile("sql/query/SelectElitePotato.sql"));
        List<Map<String, Object>> poptatoProducts = dbManager.convertResultSetToList(resultSet,"origin_price" ,"price");
        
        Float oldPrice =  (Float) poptatoProducts.get(0).get("origin_price");
        Float newPrice = (Float) poptatoProducts.get(0).get("price");
        
        //assert that potato 20 times higher price 
        assertThat(newPrice, Matchers.equalTo(oldPrice * 20));
        dbManager.closeResultSet(resultSet);
    }

    @Test
    public void getCustomersAndCompareWithCostomersFromCsvFile() {
        ResultSet resultSet = dbManager.query(FileSystemManager.readTextFile("sql/query/SelectCustomers.sql"));
        List<Map<String, Object>> dbCustomers = dbManager.convertResultSetToList(resultSet, CUSTOMER_ID, CUSTOMER_FIRST_NAME, CUSTOMER_LAST_NAME);
        CsvData csvCustomers = FileSystemManager.readCSVFileAsCsvData("csv/customers.csv", ",");
        Log.info("CSV Data", null, new CSVData(new File(FileSystemManager.getAbsolutePathToResource() + "csv/customers.csv")));
        int lineNumber = 0;
        for (CsvRow csvCustomer : csvCustomers) {
            if (rowNotEqual(csvCustomer, dbCustomers.get(lineNumber), CUSTOMER_ID) 
                    ||rowNotEqual(csvCustomer, dbCustomers.get(lineNumber), CUSTOMER_FIRST_NAME)
                    ||rowNotEqual(csvCustomer, dbCustomers.get(lineNumber), CUSTOMER_LAST_NAME)) {

                Assert.fail(csvCustomer+ " "+dbCustomers.get(lineNumber));
            }
            lineNumber += 1;
        }
    }

    private boolean rowNotEqual(CsvRow csvCustomer, Map<String, Object> dbCustomer, String key) {
        String csvData = csvCustomer.get(key).toString().replaceAll("[\\t\\n\\r]+", "");
        String dbData = dbCustomer.get(key).toString().replaceAll("[\\t\\n\\r]+", "");
        return ! csvData.equalsIgnoreCase(dbData);
    }
}
