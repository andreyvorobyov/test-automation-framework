package ta.trn.tests;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.WebDriver;

import com.sigmaukraine.report.Log;
import com.sigmaukraine.report.sourceProvider.Page;

public class Watcher extends TestWatcher {
    
    private WebDriver driver = null;
    
    public void setWebDriver(WebDriver driver){
        this.driver = driver;
    }
    
    @Override
    public void failed(Throwable e, Description description) {
        if(driver != null){
            Log.error("Error occurred", e, new Page(driver));
        }
        else{
            Log.error("Failed", e);
        }
    }

    @Override
    protected void succeeded(Description description) {
        Log.info( "Succeeded");
    }
}
