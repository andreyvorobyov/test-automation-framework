package ta.trn.tests.webapi;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hamcrest.Matchers;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import com.fasterxml.jackson.core.type.TypeReference;
import com.jayway.restassured.response.Response;
import com.sigmaukraine.report.Log;

import ta.trn.services.webapi.UriConstants;
import ta.trn.services.webapi.WebApiService;
import ta.trn.services.webapi.model.Post;
import ta.trn.tests.Watcher;

public class WebApiTest {
    
    private static WebApiService service;
    
    @Rule 
    public TestName testName = new TestName();
    
    @Rule
    public Watcher watcher = new Watcher();
    
    @BeforeClass
    public static void initService(){
        Log.openTestSuite(WebApiTest.class.getName());
        service = new WebApiService();
    }
    
    @AfterClass
    public static void closeService(){
        Log.closeTestSuite();
    }
    
    @Before
    public void init(){
        Log.openTestCase(testName.getMethodName()); 
    }
    
    @Test
    public void getListOfPostAndAssertWhatCountEqualsExpectedResult(){
        Map<String, String> parameters = new HashMap<>();
        String userId = "2";
        parameters.put("userId", userId);
        //send the request
        Response response = service.sendGetRequestWithQueryParam(UriConstants.POSTS, parameters);
        
        Assert.assertEquals(200, response.getStatusCode());
        
        //deserialize the response
        List<Post> posts = service.deserealizeFromJson(new TypeReference<List<Post>>() {}, response.asString());
        
        Assert.assertFalse(posts.isEmpty());      
    }
    
    @Test
    public void createNewPostAndAssertWhatListOfPostContainThatPost(){
        Post newPost = Post.createPost(2, "TA TRN FRM04 vorobyov", "this task TA TRN FRM04 vorobyov");        

        //create new post
        Log.openSection("Create new Post");
        String jsonText = service.serealizeToJson(newPost);
        Response responseCreatePost = service.sendPostRequest(UriConstants.POSTS, jsonText);
        
        Assert.assertEquals(201, responseCreatePost.getStatusCode());
        Log.closeSection();
        
        //update created post
        newPost = service.deserealizeFromJson(new TypeReference<Post>() {}, responseCreatePost.asString());
        
        //get actual post for user with id 2
        Log.openSection("Get actual Post for user");
        Map<String, String> parametersCountActualPosts = new HashMap<>();
        parametersCountActualPosts.put("userId", newPost.getUserId().toString());
        Response responseCountActualPosts = service.sendGetRequestWithQueryParam(UriConstants.POSTS, parametersCountActualPosts);

        Assert.assertEquals(200, responseCountActualPosts.getStatusCode());
        Log.closeSection();
        
        //deserialize the response
         List<Post> actualPosts = service.deserealizeFromJson(new TypeReference<List<Post>>() {}, responseCountActualPosts.asString());
  
         assertThat(actualPosts, hasItem(Matchers.<Post>hasProperty("id", equalTo(newPost.getId()))));
    }
    
    @Test
    public void getPostByIdAndAssertWhatTitleAndBodyEquals() {        
        //create new post without id
        Log.openSection("Create new Post");
        Post newPost = Post.createPost(2, "TA TRN FRM04 vorobyov", "this task TA TRN FRM04 vorobyov");                
        Response responseCreatePost = service.sendPostRequest(UriConstants.POSTS, service.serealizeToJson(newPost));
        
        Assert.assertEquals(201, responseCreatePost.getStatusCode());
        Log.closeSection();
        
        //deserialize created post with id
        Post createdPost = service.deserealizeFromJson(new TypeReference<Post>() {}, responseCreatePost.asString());
               
        //get created post by id
        Log.openSection("Get created Post by ID");
        List<String> parameters = Arrays.asList(createdPost.getId().toString());
        Response responsePostById = service.sendGetRequestWithPathParam(UriConstants.POSTS, parameters);
        
        Assert.assertEquals(200, responsePostById.getStatusCode());
        Log.closeSection();
        
        //deserialize got post by id
        Post gotPost = service.deserealizeFromJson(new TypeReference<Post>() {}, responsePostById.asString());
        
        Assert.assertEquals(gotPost, createdPost);
    }
    
    @Test
    public void deletePostAndAssertWhatPostDoNotGetByID() {
        //create new post without id
        Log.openSection("Create new Post");
        Post newPost = Post.createPost(2, "TA TRN FRM04 vorobyov", "this task TA TRN FRM04 vorobyov");                
        Response responseCreatePost = service.sendPostRequest(UriConstants.POSTS, service.serealizeToJson(newPost));
        
        Assert.assertEquals(201, responseCreatePost.getStatusCode());
        Log.closeSection();
        
        //deserialize created post with id
        Log.openSection("Delete created Post");
        Post createdPost = service.deserealizeFromJson(new TypeReference<Post>() {}, responseCreatePost.asString());
        
        //delete created test
        List<String> deleteParameters = Arrays.asList(createdPost.getId().toString());
        Response response = service.sendDeleteRequest(UriConstants.POSTS, deleteParameters);
        
        Assert.assertEquals(200, response.getStatusCode());
        Log.closeSection();
        
        //get created post by id must have 404 status
        Log.openSection("Get created Post by ID");
        List<String> parameters = Arrays.asList(createdPost.getId().toString());
        Response responsePostById = service.sendGetRequestWithPathParam(UriConstants.POSTS, parameters);
        
        Assert.assertEquals(404, responsePostById.getStatusCode());
        Log.closeSection();
    }
}
