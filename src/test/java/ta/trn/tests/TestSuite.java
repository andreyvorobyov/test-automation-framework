package ta.trn.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import ta.trn.tests.database.DataBaseTests;
import ta.trn.tests.webapi.WebApiTest;
import ta.trn.tests.webui.WebUiTestSuite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ 
    WebUiTestSuite.class,
    WebApiTest.class,
    DataBaseTests.class})
public class TestSuite {

    @Override
    public String toString() {
        return "TestSuite []";
    }
    
}
