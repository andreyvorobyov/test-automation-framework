package ta.trn.tests.webui;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ 
    VkLoginTest.class,
    VkPostTest.class,
    VkMessageTest.class,
    VkDoLikeTest.class})
public class WebUiTestSuite {

    @Override
    public String toString() {
        return "WebUiTestSuite []";
    }    
}
