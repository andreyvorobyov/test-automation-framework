package ta.trn.tests.webui;

import java.util.UUID;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runner.Description;
import org.openqa.selenium.WebDriver;

import com.sigmaukraine.report.Log;

import ta.trn.TestConfig;
import ta.trn.services.FileSystemManager;
import ta.trn.services.webui.WebDriverManager;
import ta.trn.services.webui.pages.HomePage;
import ta.trn.services.webui.pages.LoginPage;
import ta.trn.tests.Watcher;

public class VkMessageTest {
    
    private static WebDriver driver = null;
    private static String messageReceiverName = "Test Test";
  
    @Rule 
    public TestName testName = new TestName();

    @Rule
    public Watcher watcher = new Watcher();
    
    @BeforeClass
    public static void openBrowser() {
        Log.openTestSuite(VkMessageTest.class.getName());
        Log.openSection("Preconditions");
        try{
            driver = WebDriverManager.initDriver();
        }catch (Throwable exc){
            Watcher watcher = new Watcher();
            watcher.failed(exc, Description.createTestDescription(VkPostTest.class.getName(), "could not create web driver", ""));
            throw exc;
        }
        new LoginPage(driver)
                .open()
                .changeLocaleToEnglish()
                .enterCredentials(TestConfig.WebUI.USER_NAME, TestConfig.WebUI.PASS)
                .pressLogin();
        Log.closeSection();
    }

    @AfterClass
    public static void closeBrowser() {
        WebDriverManager.closeDriver();
        Log.closeTestSuite();
    }
    
    @Before
    public void init(){
        watcher.setWebDriver(driver);
        Log.openTestCase(testName.getMethodName()); 
    }
    
    @Test
    public void sendMessageToYourselfAndCheckDelivered() {
        String sendingText = UUID.randomUUID().toString();
        String receivedText = new HomePage(driver)
                .openMessages()
                .sendMessage(sendingText, messageReceiverName)
                .getBackToMessages()
                .openMessageFrom(messageReceiverName)
                .messageDelivered(sendingText);
        Assert.assertEquals(sendingText, receivedText);
    }

    @Test
    public void sendMessageWithAttachToYourselfAndCheckDelivered() {
        String sendingText = UUID.randomUUID().toString();
        String sendingFileName = "document.docx";
        String pathToAttach = (FileSystemManager.getAbsolutePathToResource() + "documents\\" + sendingFileName)
                .replace("/", "\\");
        String receivedFileName = new HomePage(driver)
                .openMessages()
                .sendMessage(sendingText, messageReceiverName, pathToAttach)
                .getBackToMessages()
                .openMessageFrom(messageReceiverName)
                .messageDeliveredWithAttach(sendingText);
        Assert.assertEquals(sendingFileName, receivedFileName);
    }
}
