package ta.trn.tests.webui;

import java.util.UUID;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runner.Description;
import org.openqa.selenium.WebDriver;

import com.sigmaukraine.report.Log;

import ta.trn.TestConfig;
import ta.trn.services.webui.WebDriverManager;
import ta.trn.services.webui.pages.HomePage;
import ta.trn.services.webui.pages.LoginPage;
import ta.trn.tests.Watcher;

public class VkDoLikeTest {

    private static WebDriver driver = null;
    
    @Rule 
    public TestName testName = new TestName();

    @Rule
    public Watcher watcher = new Watcher();
    
    @BeforeClass
    public static void openBrowser() {
        Log.openTestSuite(VkDoLikeTest.class.getName());
        Log.openSection("Preconditions");
        try{
            driver = WebDriverManager.initDriver();
        }catch (Throwable exc){
            Watcher watcher = new Watcher();
            watcher.failed(exc, Description.createTestDescription(VkPostTest.class.getName(), "could not create web driver", ""));
            throw exc;
        }
        new LoginPage(driver)
                .open()
                .changeLocaleToEnglish()
                .enterCredentials(TestConfig.WebUI.USER_NAME, TestConfig.WebUI.PASS)
                .pressLogin();
        Log.closeSection();
    }

    @AfterClass
    public static void closeBrowser() {
        WebDriverManager.closeDriver();
        Log.closeTestSuite();
    }

    @Before
    public void init(){
        watcher.setWebDriver(driver);
        Log.openTestCase(testName.getMethodName()); 
    }
    
    @Test
    public void likeThreeLastPostInWallIfIsNotLiked(){
        int countPostForLike = 3;
        HomePage homePage = new HomePage(driver);                
        int countPost = homePage
                .openHomePage()
                .getCountPostsOnWall();
        //create 3 posts if on wall not have 3 posts
        for(int i = countPost; i < countPostForLike; i++){
            String textToPost = UUID.randomUUID().toString();
            homePage.openHomePage()
                    .postOnWall(textToPost)
                    .getTextFromPostByNumber(homePage.getCountPostsOnWall());
        }
        
        //likes 3 posts if is did not liked
        for (int i = 0; i < countPostForLike; i++) {
            if(homePage.isNotLikedPostByNumber(i)){
                homePage.likeWallPost(i);
            }
        }

        //check liked posts
        boolean postsIsLiked = true;
        for (int i = 0; i < countPostForLike; i++) {
            if(homePage.isNotLikedPostByNumber(i)){
                postsIsLiked = false;
            }
        }
        Assert.assertTrue(postsIsLiked);        
    }   
}
