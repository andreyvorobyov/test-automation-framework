package ta.trn.tests.webui;

import static org.hamcrest.Matchers.equalTo;

import java.io.File;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.junit.rules.TestName;
import org.openqa.selenium.WebDriver;

import com.sigmaukraine.report.Log;
import com.sigmaukraine.report.sourceProvider.CSVData;

import ta.trn.services.CsvData;
import ta.trn.services.CsvRow;
import ta.trn.services.FileSystemManager;
import ta.trn.services.webui.WebDriverManager;
import ta.trn.services.webui.pages.LoginPage;
import ta.trn.tests.Watcher;

public class VkLoginTest {

    @Rule
    public ErrorCollector collector = new ErrorCollector();
    
    @Rule 
    public TestName testName = new TestName();
    
    @Rule
    public Watcher watcher = new Watcher();
    
    @BeforeClass
    public static void initService(){
        Log.openTestSuite(VkLoginTest.class.getName());
    }
    
    @AfterClass
    public static void closeService(){
        Log.closeTestSuite();
    }
    
    @Before
    public void init(){
        Log.openTestCase(testName.getMethodName()); 
    }
    
    @Test
    public void loginWithCredentialsFromCsvFile() {        
        CsvData credentialsCollection = FileSystemManager.readCSVFileAsCsvData("csv/credentials.csv", ",");
        Log.info("Credentials", null, new CSVData(new File(FileSystemManager.getAbsolutePathToResource() + "csv/credentials.csv")));
        for (CsvRow credentials : credentialsCollection) {
            String login = credentials.get("login").toString();
            String password = credentials.get("password").toString();
            WebDriver driver = WebDriverManager.initDriver();
            LoginPage loginPage = new LoginPage(driver);
            
            Boolean expectedResult = Boolean.valueOf(credentials.get("result").toString());
            
            Boolean actualResult = loginPage.open()
                    .changeLocaleToEnglish()
                    .enterCredentials(login, password)
                    .pressLogin()
                    .isLoginSuccesfull();
            
            collector.checkThat(actualResult,  equalTo(expectedResult));
            WebDriverManager.closeDriver();
        }
    }
}
