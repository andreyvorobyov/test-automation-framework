package ta.trn.tests.webui;

import java.util.UUID;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.openqa.selenium.WebDriver;

import com.sigmaukraine.report.Log;

import ta.trn.TestConfig;
import ta.trn.services.FileSystemManager;
import ta.trn.services.webui.WebDriverManager;
import ta.trn.services.webui.pages.HomePage;
import ta.trn.services.webui.pages.LoginPage;
import ta.trn.tests.Watcher;
import org.junit.runner.Description;

public class VkPostTest {

private static WebDriver driver = null;
    @Rule 
    public TestName testName = new TestName();

    @Rule
    public Watcher watcher = new Watcher();
    
    @BeforeClass
    public static void openBrowser() {
        Log.openTestSuite(VkPostTest.class.getName()); 
        Log.openSection("Preconditions");
        try{
            driver = WebDriverManager.initDriver();
        }catch (Throwable exc){
            Watcher watcher = new Watcher();
            watcher.failed(exc, Description.createTestDescription(VkPostTest.class.getName(), "could not create web driver", ""));
            throw exc;
        }
        new LoginPage(driver)
                .open()
                .changeLocaleToEnglish()
                .enterCredentials(TestConfig.WebUI.USER_NAME, TestConfig.WebUI.PASS)
                .pressLogin();
        Log.closeSection();
    }

    @AfterClass
    public static void closeBrowser() {
        WebDriverManager.closeDriver();
        Log.closeTestSuite();        
    }
    
    @Before
    public void init(){
        watcher.setWebDriver(driver);
        Log.openTestCase(testName.getMethodName()); 
    }
    
    @Test
    public void addPostOnWallAndCheckThatPostDisplayedOnWall() {
        String textToPost = UUID.randomUUID().toString();
        HomePage homePage = new HomePage(driver);
        String textFromPost = homePage
                .openHomePage()
                .postOnWall(textToPost)
                .getTextFromPostByNumber(homePage.getCountPostsOnWall());
        Assert.assertEquals(textToPost, textFromPost);
    }

    @Test
    public void addPostWithPictureOnWallAndCheckThatPostDisplayedOnWall() {
        String textToPost = UUID.randomUUID().toString();
        String imageToPost = "pic.jpg";
        String pathToImage = (FileSystemManager.getAbsolutePathToResource() + "images\\" + imageToPost).replace("/", "\\");
        HomePage homePage = new HomePage(driver);
        String textFromPost = homePage
                .openHomePage()
                .postOnWall(textToPost, pathToImage)
                .getTextFromPostByNumber(homePage.getCountPostsOnWall());
        Assert.assertEquals(textToPost, textFromPost);
    }
}
