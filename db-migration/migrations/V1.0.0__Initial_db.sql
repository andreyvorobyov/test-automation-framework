CREATE TABLE vorobyov.customers (
    customer_id INT NOT NULL AUTO_INCREMENT,
    first_name VARCHAR(20) NOT NULL,
    last_name VARCHAR(20) NOT NULL,
    PRIMARY KEY (customer_id));
    
CREATE TABLE vorobyov.products (
    product_id INT NOT NULL AUTO_INCREMENT,
    product_name VARCHAR(20) NOT NULL,
    product_group VARCHAR(20) NOT NULL,
    price FLOAT NOT NULL,
    PRIMARY KEY (product_id));
    
CREATE TABLE vorobyov.purchase_orders(
    order_id INT NOT NULL AUTO_INCREMENT,
    customer_id INT NOT NULL,
    order_date DATE NOT NULL,
    order_time TIME NOT NULL,
    PRIMARY KEY (order_id));
    
CREATE TABLE vorobyov.ordered_products(
    order_id INT,
    product_id INT,    
    amount INT)